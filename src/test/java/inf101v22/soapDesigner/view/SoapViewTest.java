package inf101v22.soapDesigner.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import inf101v22.soapDesigner.model.Design;
import inf101v22.soapDesigner.model.ModelShape;

public class SoapViewTest {

	private SoapView soapView = new SoapView(null, null);

	@Test
	void soapViewTestSaveDesign() {
		soapView.savedDesignList[0] = new Design(Color.red, Color.blue, Color.green, Color.yellow, ModelShape.model2);
		assertEquals(Color.red, soapView.savedDesignList[0].getColor(0));
		assertEquals(Color.blue, soapView.savedDesignList[0].getColor(1));
		assertEquals(Color.green, soapView.savedDesignList[0].getColor(2));
		assertEquals(Color.yellow, soapView.savedDesignList[0].getColor(3));
	}
	
	@Test
	void soapViewTestShuffle() {
		
		List<Color> colors1 = new ArrayList<Color>();
		colors1.add(Color.red);
		colors1.add(Color.blue);
		colors1.add(Color.green);
		colors1.add(Color.yellow);
		
		soapView.shuffle(Color.red, Color.blue, Color.green, Color.yellow);

		
		List<Color> colors2 = new ArrayList<Color>();
		colors2.add(soapView.c1);
		colors2.add(soapView.c2);
		colors2.add(soapView.c3);
		colors2.add(soapView.c4);
	
		assertFalse(colors1.equals(colors2));

	}
}
