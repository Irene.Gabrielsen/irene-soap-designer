package inf101v22.soapDesigner.model;

import java.awt.Color;

public class Design {
	
	private ModelShape model = ModelShape.model1;
	private Color[] colors = new Color[4];

	/**
	 *  Design holds the 5 values needed to save a design.
	 *  The Design objects are created and added to the list savedDesignList[] in SoapView
	 *  when a design is saved.
	 *  
	 * @param col1
	 * @param col2
	 * @param col3
	 * @param col4
	 * @param selectedModel
	 */
	public Design(Color col1, Color col2, Color col3, Color col4, ModelShape selectedModel) {
		colors[0] = col1;
		colors[1] = col2;
		colors[2] = col3;
		colors[3] = col4;
		this.model = selectedModel;
	}
	
	/**
	 * Alternative Design constructor with the colors in an array.
	 * 
	 * @param colors
	 * @param selectedModel
	 */
	public Design(Color[] colors, ModelShape selectedModel) {
		this.colors = colors;
		model = selectedModel;
	}
	
	/**
	 * setColors updates the four colors in Design to match the colors in SoapView.
	 * 
	 * @param c1
	 * @param c2
	 * @param c3
	 * @param c4
	 */
	public void setColors(Color c1, Color c2, Color c3, Color c4) {
		colors[0] = c1;
		colors[1] = c2;
		colors[2] = c3;
		colors[3] = c4;
	}

	/**
	 * setModel updates the active model in Design to match the active model in SoapView.
	 * @param selectedModel
	 */
	public void setModel(ModelShape selectedModel) {
		model = selectedModel;
	}

	/**
	 * getColor returns the color with the given index (parameter).
	 * 
	 * @param i
	 * @return
	 */
	public Color getColor(int i) {
		return colors[i];
	}
	
	/**
	 * @return the Designs model
	 */
	public ModelShape getModel() {
		return model;
	}
}
