package inf101v22.soapDesigner.model;

public class ModelShape {
	
	int[][] model;

	/**
	 *  ModelShape constructor creates a model as a 2D array.
	 *  
	 *  Fill the arrays with numbers 1, 2, 3, 4 and 11, 22, 33, 44 to make use of Soap Designers colorscheme.
	 *  In black/white the double numbers will be black.
	 *  In color format the numbers made of the same digits will be the same color, example 2 = 22.
	 *  
	 * @param model
	 */
	private ModelShape(int[][] model) {
		this.model = model;		
	}
	

	// List of the models currently in the program:
	
	public static final ModelShape model1 = new ModelShape(new int[][] {{ 11, 11, 11, 11, 11, 11 },
																{ 3, 3, 22, 2, 2, 2 },
																{ 3, 3, 3, 22, 2, 2 },
																{ 3, 3, 3, 3, 22, 2 },
																{ 3, 3, 3, 3, 3, 22 },
																{ 33, 33, 33, 33, 33, 33 },
																{ 4, 4, 4, 4, 4, 4 },
																{ 4, 4, 4, 4, 4, 4 }});
	
	public static final ModelShape model2 = new ModelShape(new int[][] {{ 11, 1, 1, 1, 1, 11 },
																{ 2, 11, 1, 1, 11, 2 },
																{ 2, 2, 11, 11, 2, 2 },
																{ 2, 2, 2, 2, 2, 2 },
																{ 2, 2, 22, 22, 2, 2 },
																{ 2, 22, 3, 3, 22, 2 },
																{ 22, 3, 3, 3, 3, 22 },
																{ 3, 3, 3, 3, 3, 3 }});
	
	public static final ModelShape model3 = new ModelShape(new int[][] {{ 1, 1, 1, 11, 2, 2 },
																{ 1, 1, 11, 2, 2, 2 },
																{ 1, 11, 2, 2, 2, 2 },
																{ 11, 2, 2, 2, 2, 2 },
																{ 2, 2, 2, 2, 2, 22 },
																{ 2, 2, 2, 2, 22, 3 },
																{ 2, 2, 2, 22, 3, 3 },
																{ 2, 2, 22, 3, 3, 3 }});
	
	public static final ModelShape model4 = new ModelShape(new int[][] {{ 1, 1, 1, 1, 1, 1 },
																{ 1, 11, 1, 1, 1, 1 },
																{ 11, 2, 11, 1, 1, 1 },
																{ 2, 2, 2, 11, 1, 11 },
																{ 2, 2, 2, 2, 11, 2 },
																{ 2, 2, 2, 2, 2, 2 },
																{ 2, 2, 2, 2, 2, 2 },
																{ 2, 2, 2, 2, 2, 2 }});

	public static final ModelShape model5 = new ModelShape(new int[][] {{ 1, 1, 1, 1, 1, 1 },
																{ 11, 11, 11, 11, 11, 11 },
																{ 2, 2, 2, 22, 22, 2 },
																{ 22, 2, 22, 3, 3, 22 },
																{ 3, 22, 3, 3, 3, 3 },
																{ 33, 33, 33, 33, 33, 33 },
																{ 4, 4, 4, 4, 4, 4 },
																{ 44, 44, 44, 44, 44, 44 }});

	public static final ModelShape model6 = new ModelShape(new int[][] {{ 11, 1, 1, 1, 1, 1 },
																{ 2, 11, 1, 1, 1, 11 },
																{ 2, 2, 11, 11, 11, 2 },
																{ 22, 2, 2, 2, 2, 2 },
																{ 3, 22, 2, 2, 2, 2 },
																{ 3, 22, 2, 2, 2, 2 },
																{ 3, 3, 22, 2, 2, 2 },
																{ 3, 3, 3, 22, 2, 2 }});
		
	
	// An array containing the available model choices
	public static final ModelShape[] MODELS = {model1, model2, model3, model4, model5, model6};
	
	
	/**
	 * @return the Pixel (int) value of the requested coordinate
	 */
	public int getPixel(int col, int row) {
		int pixel = model[col][row];
		return pixel;
	}

}
