package inf101v22.soapDesigner.designSetup;

import java.util.ArrayList;
import inf101v22.grid.Grid;

public class BackgroundBoard extends Grid<Pixel>{

	private ArrayList<Pixel> board;
	
	/**
	 * BackgroundBoard is an extension of Grid, and it sets up a grid with given inital pixelvalues
	 */
	public BackgroundBoard(int cols, int rows, Pixel initialValue ) {
		super(rows, cols, initialValue);
		board = new ArrayList<Pixel>(rows*cols);
		for (int i=0; i<rows*cols; ++i) {
			board.add(initialValue);			
		}	
	}
}