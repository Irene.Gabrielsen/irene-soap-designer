package inf101v22.soapDesigner.designSetup;

import inf101v22.grid.Coordinate;
import inf101v22.grid.CoordinateItem;
import inf101v22.soapDesigner.model.ModelShape;
import inf101v22.soapDesigner.view.Viewable;

public class ModelPresentations implements Viewable {
	
	private BackgroundBoard models;			
	private BackgroundBoard design;			
	private BackgroundBoard savedDesigns;	
	private ModelShape activeDesign = ModelShape.MODELS[0];
	
	private ModelShape savedDesign0 = ModelShape.MODELS[0];
	private ModelShape savedDesign1 = ModelShape.MODELS[2];
	private ModelShape savedDesign2 = ModelShape.MODELS[3];
	private ModelShape savedDesign3 = ModelShape.MODELS[4];
	private ModelShape savedDesign4 = ModelShape.MODELS[0];
	private ModelShape savedDesign5 = ModelShape.MODELS[0];
	private ModelShape savedDesign6 = ModelShape.MODELS[0];
	private ModelShape savedDesign7 = ModelShape.MODELS[0];
	private ModelShape savedDesign8 = ModelShape.MODELS[0];
	private ModelShape savedDesign9 = ModelShape.MODELS[0];


	/**
	 *  ModelPresentation sets up the three backgroundboards in the window.
	 */
	public ModelPresentations() {
		
		models = new BackgroundBoard(120,8, new Pixel(0));		// Background for the models that can be selected.
		design = new BackgroundBoard(6, 8, new Pixel(0));		// Background for the active design in the middle.
		savedDesigns = new BackgroundBoard(80,8, new Pixel(0));	// Background for the 10 saved designs at the bottom.
		
		// Adding the 6 static models that the user can click to select:
		model(ModelShape.MODELS[0], 10);
		model(ModelShape.MODELS[1], 17);
		model(ModelShape.MODELS[2], 24);
		model(ModelShape.MODELS[3], 31);
		model(ModelShape.MODELS[4], 38);
		model(ModelShape.MODELS[5], 45);

		// Adding the active design being worked on currently:
		activeDesign(activeDesign);

	}	
	
	/**
	 * model iterates through the provided model shape, and sets the pixel value for
	 * each coordinate.
	 * 
	 * displacement indicates how far to the left the model should be drawn, each
	 * model should be displaced by 7 more than the previous one, for symmetry.
	 * 
	 * @param model
	 * @param displacement
	 */
	private void model(ModelShape model, int displacement) {
		for (int i = 0; i<8; i++) {
			for (int j = 0; j<6; j++) {
				int pix = model.getPixel(i, j);
				models.set(new Coordinate(i,j+displacement), new Pixel(pix));
			}	
		}
	}
	
	/**
	 * activeDesign iterates through the provided model shape (which is the active model),
	 * and sets the pixel value for each coordinate.
	 * 
	 * @param model
	 */
	private void activeDesign(ModelShape model) {
		for (int i = 0; i<8; i++) {
			for (int j = 0; j<6; j++) {
				int pix = model.getPixel(i, j);
				design.set(new Coordinate(i,j), new Pixel(pix));
			}	
		}
	}
	
	/**
	 * savedDesign iterates through the provided model shape (of a saved model),
	 * and sets the pixel value for each coordinate.
	 * 
	 * displacement indicates how far to the left the model should be drawn, each
	 * model should be displaced by 8 more than the previous one, for symmetry.
	 * 
	 * @param savedDesign
	 * @param displacement
	 */
	private void savedDesign(ModelShape savedDesign, int displacement) {
		for (int i = 0; i<8; i++) {
			for (int j = 0; j<6; j++) {
				int pix = savedDesign.getPixel(i, j);
				savedDesigns.set(new Coordinate(i,j+displacement), new Pixel(pix));
			}
		}
	}
	
	/**
	 * setActiveModel sets the activeDesign variable to be equal to the currently
	 * active model in SoapView.
	 * 
	 * @param model
	 */
	public void setActiveModel(ModelShape model) {
		this.activeDesign = model;
		activeDesign(activeDesign);		
	}
	

	@Override
	public Iterable<CoordinateItem<Pixel>> savedOnBoard() {
		return savedDesigns;
	}
	
	@Override
	public Iterable<CoordinateItem<Pixel>> activeDesign() {
		return design;
	}

	@Override
	public Iterable<CoordinateItem<Pixel>> modelsOnBoard() {
		return models;
	}

	
	/**
	 * setSavedDesign places a saved design on the savedDesigns background
	 * 
	 * There is one method for each of the 10 slots for saved designs, and I could
	 * probably find a solution so that only one method is needed, but i have ran
	 * out of time, so it stays like this for now.
	 * 
	 * @param model
	 */
	public void setSavedDesign0(ModelShape model) {
		this.savedDesign0 = model;
		savedDesign(savedDesign0, 0);		
	}
	/**
	 * see documentation for setSavedDesign0
	 * @param model
	 */
	public void setSavedDesign1(ModelShape model) {
		this.savedDesign1 = model;
		savedDesign(savedDesign1, 8);		
	}
	/**
	 * see documentation for setSavedDesign0
	 * @param model
	 */
	public void setSavedDesign2(ModelShape model) {
		this.savedDesign2 = model;
		savedDesign(savedDesign2, 16);		
	}	
	/**
	 * see documentation for setSavedDesign0
	 * @param model
	 */
	public void setSavedDesign3(ModelShape model) {
		this.savedDesign3 = model;
		savedDesign(savedDesign3, 24);		
	}
	/**
	 * see documentation for setSavedDesign0
	 * @param model
	 */
	public void setSavedDesign4(ModelShape model) {
		this.savedDesign4 = model;
		savedDesign(savedDesign4, 32);		
	}
	/**
	 * see documentation for setSavedDesign0
	 * @param model
	 */
	public void setSavedDesign5(ModelShape model) {
		this.savedDesign5 = model;
		savedDesign(savedDesign5, 40);		
	}
	/**
	 * see documentation for setSavedDesign0
	 * @param model
	 */
	public void setSavedDesign6(ModelShape model) {
		this.savedDesign6 = model;
		savedDesign(savedDesign6, 48);		
	}
	/**
	 * see documentation for setSavedDesign0
	 * @param model
	 */
	public void setSavedDesign7(ModelShape model) {
		this.savedDesign7 = model;
		savedDesign(savedDesign7, 56);		
	}
	/**
	 * see documentation for setSavedDesign0
	 * @param model
	 */
	public void setSavedDesign8(ModelShape model) {
		this.savedDesign8 = model;
		savedDesign(savedDesign8, 64);		
	}
	/**
	 * see documentation for setSavedDesign0
	 * @param model
	 */
	public void setSavedDesign9(ModelShape model) {
		this.savedDesign9 = model;
		savedDesign(savedDesign9, 72);		
	}
}
