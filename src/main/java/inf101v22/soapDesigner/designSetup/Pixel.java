package inf101v22.soapDesigner.designSetup;

public class Pixel {

	public int colorCode;
	
	/**
	 * Pixel constructor creates a pixel with a colorCode.
	 * 
	 * @param colorCode
	 */
	public Pixel(int colorCode) {
		this.colorCode = colorCode;		
	}

}
