package inf101v22.soapDesigner;

import javax.swing.JComponent;
import javax.swing.JFrame;
import inf101v22.soapDesigner.designSetup.ModelPresentations;
import inf101v22.soapDesigner.view.SoapView;

public class Main {
    public static final String WINDOW_TITLE = "Soap Designer";

    public static void main(String[] args) {
    	
    	ModelPresentations modelPresentation = new ModelPresentations();
    	JComponent SoapView = new SoapView(modelPresentation, modelPresentation);
    	
		JComponent view = SoapView;
        
        JFrame frame = new JFrame(WINDOW_TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(view);
        frame.pack();
        frame.setVisible(true);
    }
    
}
