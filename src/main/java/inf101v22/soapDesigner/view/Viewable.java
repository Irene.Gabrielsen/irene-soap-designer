package inf101v22.soapDesigner.view;

import inf101v22.grid.CoordinateItem;
import inf101v22.soapDesigner.designSetup.Pixel;

public interface Viewable {
	
	/**
	 * activeDesign gives an iterable of the pixels in the active design
	 * 
	 * @return
	 */
	public Iterable<CoordinateItem<Pixel>> activeDesign();

	/**
	 * modelsOnBoard gives an iterable of the clickable models
	 * 
	 * @return
	 */
	public Iterable<CoordinateItem<Pixel>> modelsOnBoard();
	
	/**
	 * savedOnBoard gives an iterable of the saved designs
	 * 
	 * @return
	 */
	public Iterable<CoordinateItem<Pixel>> savedOnBoard();



}
