package inf101v22.soapDesigner.view;

import javax.swing.JComponent;
import inf101v22.grid.CoordinateItem;
import inf101v22.soapDesigner.designSetup.ModelPresentations;
import inf101v22.soapDesigner.designSetup.Pixel;
import inf101v22.soapDesigner.model.ModelShape;
import inf101v22.soapDesigner.model.Design;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SoapView extends JComponent implements MouseListener {
	private static final long serialVersionUID = 1L;

	{
    this.setFocusable(true);
    }
    
	private Viewable board;
	private ModelPresentations modelPresentations;
    
    // CUSTOM FONTS //
    private Font myFont = new Font("SansSerif", Font.BOLD, 30);
    private Font myFont1 = new Font("SansSerif", Font.BOLD, 18);
    private Font myFont2 = new Font("SansSerif", Font.BOLD, 11);
    private Font myFont3 = new Font("SansSerif", Font.BOLD, 14);

    // CUSTOM COLORS //
    private Color teal = (new Color(0xa7, 0xe1, 0xcf));
    private Color lightGray = (new Color(0x8d,0x8d,0x8d));
    private Color medGray = (new Color(0x5d,0x5d,0x5d));
    private Color darkGray = (new Color(0x2d,0x2d,0x2d));
    private Color natural = (new Color(0xFD,0xF5,0xE6));
    
    // USER CHOICES //
    private ModelShape activeDesign = ModelShape.MODELS[0];
    private Color activeColor = natural;
    Color c1 = natural; // c1, c2, c3 and c4 are package private in order to be used in a test
    Color c2 = natural;
    Color c3 = natural;
    Color c4 = natural; 
    
	// Default values for the list of saved designs
    // (savedDesignList is package private in order to be used in a test)
    Design[] savedDesignList = 
    	{(new Design(natural, natural, natural, natural, activeDesign)),
         (new Design(natural, natural, natural, natural, activeDesign)),
         (new Design(natural, natural, natural, natural, activeDesign)),
         (new Design(natural, natural, natural, natural, activeDesign)),
         (new Design(natural, natural, natural, natural, activeDesign)),
         (new Design(natural, natural, natural, natural, activeDesign)),
         (new Design(natural, natural, natural, natural, activeDesign)),
         (new Design(natural, natural, natural, natural, activeDesign)),
         (new Design(natural, natural, natural, natural, activeDesign)),
         (new Design(natural, natural, natural, natural, activeDesign))};

    // COLORS FOR SWATCHES ROW 1 //
    private Color c11 = (new Color(0xFF, 0x33, 0x33));
    private Color c12 = (new Color(0xFF, 0x66, 0x66));
    private Color c13 = (new Color(0xFF, 0x99, 0x99));
    private Color c14 = (new Color(0xff, 0xcc, 0xcc));
    private Color c15 = (new Color(0xff, 0xff, 0x4d)); 
    private Color c16 = (new Color(0xff, 0xff, 0x99));
    private Color c17 = (new Color(0xff, 0xff, 0xcc));
    private Color c18 = (new Color(0xFD,0xF5,0xE6));
    private Color c19 = (new Color(0xF7, 0xC0, 0x6E));
    private Color c10 = (new Color(0x99, 0x66, 0x33));
    
    // COLORS FOR SWATCHES ROW 2 //
    private Color c21 = (new Color(0x00, 0x66, 0xff));
    private Color c22 = (new Color(0x4d, 0x94, 0xff)); 
    private Color c23 = (new Color(0x99, 0xc2, 0xff));
    private Color c24 = (new Color(0xcc, 0xe0, 0xff));
    private Color c25 = (new Color(0x33,0x99,0x33));
    private Color c26 = (new Color(0x66,0xcc,0x66));
    private Color c27 = (new Color(0x9f,0xdf,0x9f));
    private Color c28 = (new Color(0xd9,0xf2,0xd9));
    private Color c29 = lightGray;
    private Color c20 = (new Color(0x36,0x36,0x36));
    
    
    /**
     * Constructor for SoapView
     * 
     * @param board
     * @param modelPres
     */
    public SoapView(Viewable board, ModelPresentations modelPres) {
    	this.modelPresentations = modelPres;
    	this.board = board;
        addMouseListener(this);
    }

    @Override
    public Dimension getPreferredSize() {
        int width = 1250;
        int height = 980;
        return new Dimension(width, height);
    }
	
    /**
     * shuffle randomly shuffles the 4 chosen colors.
     * 
     * @param C1
     * @param C2
     * @param C3
     * @param C4
     */
	void shuffle(Color C1, Color C2, Color C3, Color C4) {
		List<Color> colorShuffle = new ArrayList<Color>();
		colorShuffle.add(C1);
		colorShuffle.add(C2);
		colorShuffle.add(C3);
		colorShuffle.add(C4);
		Collections.shuffle(colorShuffle);

		this.c1 = colorShuffle.get(0);
		this.c2 = colorShuffle.get(1);
		this.c3 = colorShuffle.get(2);
		this.c4 = colorShuffle.get(3);
	}
	
	void drawButton(Graphics g, int x, int y, int width, int height, Color border, Color fill) {
		g.setColor(border);
		g.fillRect(x, y, width, height);
		g.setColor(fill);
		g.fillRect(x+2, y+2, width-4, height-4);
	}
	
	

    @Override
    public void paint(Graphics canvas) {
        super.paint(canvas);
               
        //// STATIC BACKGROUND ////
        canvas.setColor(teal);
        canvas.fillRect(20, 20, 1210, 40);
        canvas.setColor(lightGray);
        canvas.fillRect(20, 60, 1210,650);
        canvas.fillRect(20, 842, 1210,116);
        canvas.setColor(medGray);
        canvas.fillRect(20, 180, 1210, 2);
        canvas.fillRect(20, 620, 1210, 2);
        canvas.fillRect(20, 676, 1210, 2);
        
        // The model choices (patters)
        drawModelSelection(canvas, 20, 80);
        
        // Backgrounds to drawn objects
        canvas.setColor(Color.white);
        canvas.fillRect(480, 180, 340, 440); 	// Behind active design
        canvas.fillRect(35, 200, 52, 52); 		// Behind active color
        canvas.fillRect(125, 200, 52, 52); 		// Behind the four color choices
        canvas.fillRect(182, 200, 52, 52);
        canvas.fillRect(239, 200, 52, 52);
        canvas.fillRect(296, 200, 52, 52);
        canvas.fillRect(34, 271, 202, 22); 		// Behind the colorswatch 1
        canvas.fillRect(34, 295, 202, 22);		// Behind the colorswatch 2
        
        // Shuffle button
 		drawButton(canvas, 375, 205, 80, 40, darkGray, Color.white);
 
        // The 10 Save Here buttons
        drawButton(canvas, 74, 680, 88, 26, darkGray, Color.white);
        drawButton(canvas, 186, 680, 88, 26, darkGray, Color.white);
        drawButton(canvas, 298, 680, 88, 26, darkGray, Color.white);
        drawButton(canvas, 410, 680, 88, 26, darkGray, Color.white);
        drawButton(canvas, 522, 680, 88, 26, darkGray, Color.white);
        drawButton(canvas, 634, 680, 88, 26, darkGray, Color.white);
        drawButton(canvas, 746, 680, 88, 26, darkGray, Color.white);
        drawButton(canvas, 858, 680, 88, 26, darkGray, Color.white);
        drawButton(canvas, 970, 680, 88, 26, darkGray, Color.white);
        drawButton(canvas, 1082, 680, 88, 26, darkGray, Color.white);
        
        // The 10 Edit Design buttons
        drawButton(canvas, 74, 850, 88, 26, darkGray, Color.white);
        drawButton(canvas, 186, 850, 88, 26, darkGray, Color.white);
        drawButton(canvas, 298, 850, 88, 26, darkGray, Color.white);
        drawButton(canvas, 410, 850, 88, 26, darkGray, Color.white);
        drawButton(canvas, 522, 850, 88, 26, darkGray, Color.white);
        drawButton(canvas, 634, 850, 88, 26, darkGray, Color.white);
        drawButton(canvas, 746, 850, 88, 26, darkGray, Color.white);
        drawButton(canvas, 858, 850, 88, 26, darkGray, Color.white);
        drawButton(canvas, 970, 850, 88, 26, darkGray, Color.white);
        drawButton(canvas, 1082, 850, 88, 26, darkGray, Color.white);


        // COLOR PALETTE //
        // Colorswatch line 1
        canvas.setColor(c11);
        canvas.fillRect(35, 272, 20, 20); 
        canvas.setColor(c12);
        canvas.fillRect(55, 272, 20, 20);
        canvas.setColor(c13);
        canvas.fillRect(75, 272, 20, 20);
        canvas.setColor(c14);
        canvas.fillRect(95, 272, 20, 20);
        canvas.setColor(c15);
        canvas.fillRect(115, 272, 20, 20);    
        canvas.setColor(c16);
        canvas.fillRect(135, 272, 20, 20);
        canvas.setColor(c17);
        canvas.fillRect(155, 272, 20, 20);
        canvas.setColor(c18);
        canvas.fillRect(175, 272, 20, 20);
        canvas.setColor(c19);
        canvas.fillRect(195, 272, 20, 20);
        canvas.setColor(c10);
        canvas.fillRect(215, 272, 20, 20);
        
        // Colorswatch line 2
        canvas.setColor(c21);
        canvas.fillRect(35, 296, 20, 20);    
        canvas.setColor(c22); 
        canvas.fillRect(55, 296, 20, 20); 
        canvas.setColor(c23);
        canvas.fillRect(75, 296, 20, 20); 
        canvas.setColor(c24);
        canvas.fillRect(95, 296, 20, 20); 
        canvas.setColor(c25);
        canvas.fillRect(115, 296, 20, 20);
        canvas.setColor(c26);
        canvas.fillRect(135, 296, 20, 20);
        canvas.setColor(c27);
        canvas.fillRect(155, 296, 20, 20); 
        canvas.setColor(c28);
        canvas.fillRect(175, 296, 20, 20);
        canvas.setColor(c29);
        canvas.fillRect(195, 296, 20, 20); 
        canvas.setColor(c20);
        canvas.fillRect(215, 296, 20, 20);

        // Active color display
        canvas.setColor(activeColor);
        canvas.fillRect(36, 201, 50, 50);
        
        // The four chosen colors
        canvas.setColor(c1);
        canvas.fillRect(126, 201, 50, 50);
        canvas.setColor(c2);
        canvas.fillRect(183, 201, 50, 50);
        canvas.setColor(c3);
        canvas.fillRect(240, 201, 50, 50);
        canvas.setColor(c4);
        canvas.fillRect(297, 201, 50, 50);
        
        // The active design being worked on
        drawActiveDesign(canvas, 500, 200);
        // The saved designs at the bottom
        drawSavedDesigns(canvas, 75, 720);
        
        
        // Text areas
        canvas.setColor(darkGray); 
        canvas.setFont(myFont);
        canvas.drawString("Soap Designer", 502, 50);
        canvas.setFont(myFont1);
        canvas.drawString("Choose", 35, 110);
        canvas.drawString("a pattern", 35, 130);
        canvas.setFont(myFont);
        canvas.drawString(">", 100, 235);
        canvas.setFont(myFont2);
        canvas.drawString("SET", 141, 248);
        canvas.drawString("SET", 198, 248);
        canvas.drawString("SET", 255, 248);
        canvas.drawString("SET", 312, 248);
        canvas.setFont(myFont1);
        canvas.drawString("Shuffle", 385, 230);

        
        canvas.setFont(myFont3);        
        canvas.drawString("Save Here", 81, 697);
        canvas.drawString("Save Here", 193, 697);
        canvas.drawString("Save Here", 305, 697);
        canvas.drawString("Save Here", 417, 697);
        canvas.drawString("Save Here", 529, 697);
        canvas.drawString("Save Here", 641, 697);
        canvas.drawString("Save Here", 753, 697);
        canvas.drawString("Save Here", 865, 697);
        canvas.drawString("Save Here", 977, 697);
        canvas.drawString("Save Here", 1089, 697);
          
        canvas.drawString("Edit Design", 78, 868);
        canvas.drawString("Edit Design", 190, 868);
        canvas.drawString("Edit Design", 302, 868);
        canvas.drawString("Edit Design", 414, 868);
        canvas.drawString("Edit Design", 526, 868);
        canvas.drawString("Edit Design", 638, 868);
        canvas.drawString("Edit Design", 750, 868);
        canvas.drawString("Edit Design", 862, 868);
        canvas.drawString("Edit Design", 974, 868);
        canvas.drawString("Edit Design", 1086, 868);
        
    }  
    
    /**
     * drawPixelsSavedColors is called when a saved model is to be drawn. The parameters
     * provide the colors of that model specifically, as well as the color code, height,
     * width, and location to be drawn in (x, y).
     * 
     * @param g
     * @param x
     * @param y
     * @param width
     * @param height
     * @param colorCode
     * @param col1
     * @param col2
     * @param col3
     * @param col4
     */
    private void drawPixelsSavedColors(Graphics g, int x, int y, int width, int height, int colorCode, Color col1, Color col2, Color col3, Color col4) {
    	Color pixelColor = natural;    	
    	if (colorCode == 1 || colorCode ==11) {
    		pixelColor = col1;
    	}
    	else if (colorCode == 2 || colorCode ==22) {
    		pixelColor = col2;
    	}
    	else if (colorCode == 3 || colorCode ==33) {
    		pixelColor = col3;
    	}
    	else if (colorCode == 4 || colorCode ==44) {
    		pixelColor = col4;
    	}
		g.setColor(pixelColor);
    	g.fillRect(x, y, width, height);
    }

    /**
     * drawSavedDesigns draws a saved design by iterating through it and calling drawPixelsSavedColors
     * at the end of each iteration. It calculates which colors and model to use based on the
     * index of the iteration.
     * 
     * @param g
     * @param xLocSaved
     * @param yLocSaved
     */
    private void drawSavedDesigns(Graphics g, int xLocSaved, int yLocSaved) { 	
    	int i = 0;
    	for (CoordinateItem<Pixel> coordPixel : board.savedOnBoard()) {   
    		
    		int column = i%80; // Calculating column by the modulus of the index, by total 80 columns    		
    		int p = (int) column/8; // Calculating the saved designs index by splitting comumns in to groups of 8
    		
    		Color col1 = savedDesignList[p].getColor(0); 
    		Color col2 = savedDesignList[p].getColor(1); 
    		Color col3 = savedDesignList[p].getColor(2); 
    		Color col4 = savedDesignList[p].getColor(3);
    		
    		Pixel pixel = coordPixel.get();
    		int colorCode = pixel.colorCode;
    		int col = coordPixel.coordinate.col;
    		int row = coordPixel.coordinate.row; 
    		int x = xLocSaved + col * 14;
    		int y = yLocSaved + row * 14;
    		int height = 14;
    		int width = 14;
    		i++;
    		drawPixelsSavedColors(g, x, y, width, height, colorCode, col1, col2, col3, col4);
    	}
    }

    /**
     * drawPixelsCurrentColor uses the currently selected 4 colors, and colors the pixel accordingly.
     * 
     * When colors are used, the colorCode 1 is equal to 11, 2 is equal to 22, and so on.
     * 
     * @param g
     * @param x
     * @param y
     * @param width
     * @param height
     * @param colorCode
     */
    private void drawPixelsCurrentColor(Graphics g, int x, int y, int width, int height, int colorCode) {
    	Color pixelColor = Color.white;    	
    	if (colorCode == 1 || colorCode ==11) {
    		pixelColor = c1;
    	}
    	else if (colorCode == 2 || colorCode ==22) {
    		pixelColor = c2;
    	}
    	else if (colorCode == 3 || colorCode ==33) {
    		pixelColor = c3;
    	}
    	else if (colorCode == 4 || colorCode ==44) {
    		pixelColor = c4;
    	}
		g.setColor(pixelColor);
    	g.fillRect(x, y, width, height);
    }
    
    /**
     * drawActiveDesign iterates through the pixels of the active design, gets the colorCode for
     * each pixel, and calls drawPixelsCurrentColor for each of them.
     * 
     * @param g
     * @param xLocDesign
     * @param yLocDesign
     */
    private void drawActiveDesign(Graphics g, int xLocDesign, int yLocDesign) {
    	for (CoordinateItem<Pixel> coordPixel : board.activeDesign()) {
    		int col = coordPixel.coordinate.col;
			int row = coordPixel.coordinate.row;
			Pixel pixel = coordPixel.get();
			int colorCode = pixel.colorCode;
			int x = xLocDesign + col * 50;
			int y = yLocDesign + row * 50;
			int height = 50;
			int width = 50;
			drawPixelsCurrentColor(g, x, y, width, height, colorCode); 
    		}
		}
    
    /**
     * drawBlackWhitePixels will color a pixel either black or white, depending on its colorCode.
     * If the digit is single, the color is set to white, and if the digit is double, the
     * color is set to black.
     * 
     * This is used to display the patterns.
     * 
     * @param g
     * @param x
     * @param y
     * @param width
     * @param height
     * @param colorCode
     */
    private void drawBlackWhitePixels(Graphics g, int x, int y, int width, int height, int colorCode) {
    	Color pixelColor = lightGray;    	
    	if (colorCode == 11 || colorCode ==22 || colorCode ==33 || colorCode ==44) {
    		pixelColor = Color.black;
    	}
    	else if (colorCode == 1 || colorCode ==2 || colorCode ==3 || colorCode ==4){
    		pixelColor = Color.white;
    	}
		g.setColor(pixelColor);
    	g.fillRect(x, y, width, height);
    }

    /**
     * drawModelSelection will draw the static models in the top of the window, and
     * color them black and white in order to just show the pattern.
     * 
     * It does so by iterating through the pixels of each model, and calling
     * drawBlackWhitePixels at the end of each iteration.
     * 
     * @param g
     * @param xLocBoard
     * @param yLocBoard
     */
	private void drawModelSelection (Graphics g, int xLocBoard, int yLocBoard){
		for (CoordinateItem<Pixel> coordPixel : board.modelsOnBoard()) {
			int row = coordPixel.coordinate.row;
			int col = coordPixel.coordinate.col;
			Pixel pixel = coordPixel.get();
			int colorCode = pixel.colorCode;
			int x = xLocBoard + col * 10;
			int y = yLocBoard + row * 10;
			int height = 10;
			int width = 10;			
			drawBlackWhitePixels(g, x, y, width, height, colorCode); 
		}
   	}	
	   
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		
		//// SHUFFLE BUTTON ////	
        
        if (e.getX() > 377 && e.getX() < 453 && e.getY() > 207 && e.getY() < 243) {
        	shuffle(c1,c2,c3,c4);
        	repaint();
        }


        //// EDIT BUTTONS ////
        
        else if (e.getX() > 74 && e.getX() < 162 && e.getY() > 850 && e.getY() < 876) {
        	this.activeDesign = savedDesignList[0].getModel();
        	modelPresentations.setActiveModel(savedDesignList[0].getModel());
        	this.c1 = savedDesignList[0].getColor(0); 
        	this.c2 = savedDesignList[0].getColor(1);
        	this.c3 = savedDesignList[0].getColor(2); 
        	this.c4 = savedDesignList[0].getColor(3);
        	repaint();
        	System.out.println("Editing Design 0");
        } 
        else if (e.getX() > 186 && e.getX() < 274 && e.getY() > 850 && e.getY() < 876) {
        	this.activeDesign = savedDesignList[1].getModel();
        	modelPresentations.setActiveModel(savedDesignList[1].getModel());
        	this.c1 = savedDesignList[1].getColor(0); 
        	this.c2 = savedDesignList[1].getColor(1);
        	this.c3 = savedDesignList[1].getColor(2); 
        	this.c4 = savedDesignList[1].getColor(3);
        	repaint();
        	System.out.println("Editing Design 1");
        }         
        else if (e.getX() > 298 && e.getX() < 386 && e.getY() > 850 && e.getY() < 876) {
        	this.activeDesign = savedDesignList[2].getModel();
        	modelPresentations.setActiveModel(savedDesignList[2].getModel());
        	this.c1 = savedDesignList[2].getColor(0); 
        	this.c2 = savedDesignList[2].getColor(1);
        	this.c3 = savedDesignList[2].getColor(2); 
        	this.c4 = savedDesignList[2].getColor(3);
        	repaint();
        	System.out.println("Editing Design 2");
        }  
        else if (e.getX() > 410 && e.getX() < 498 && e.getY() > 850 && e.getY() < 876) {
        	this.activeDesign = savedDesignList[3].getModel();
        	modelPresentations.setActiveModel(savedDesignList[3].getModel());
        	this.c1 = savedDesignList[3].getColor(0); 
        	this.c2 = savedDesignList[3].getColor(1);
        	this.c3 = savedDesignList[3].getColor(2); 
        	this.c4 = savedDesignList[3].getColor(3);
        	repaint();
        	System.out.println("Editing Design 3");
        }  
        else if (e.getX() > 522 && e.getX() < 610 && e.getY() > 850 && e.getY() < 876) {
        	this.activeDesign = savedDesignList[4].getModel();
        	modelPresentations.setActiveModel(savedDesignList[4].getModel());
        	this.c1 = savedDesignList[4].getColor(0); 
        	this.c2 = savedDesignList[4].getColor(1);
        	this.c3 = savedDesignList[4].getColor(2); 
        	this.c4 = savedDesignList[4].getColor(3);
        	repaint();
        	System.out.println("Editing Design 4");
        }  
        else if (e.getX() > 634 && e.getX() < 722 && e.getY() > 850 && e.getY() < 876) {
        	this.activeDesign = savedDesignList[5].getModel();
        	modelPresentations.setActiveModel(savedDesignList[5].getModel());
        	this.c1 = savedDesignList[5].getColor(0); 
        	this.c2 = savedDesignList[5].getColor(1);
        	this.c3 = savedDesignList[5].getColor(2); 
        	this.c4 = savedDesignList[5].getColor(3);
        	repaint();
        	System.out.println("Editing Design 5");
        }  
        else if (e.getX() > 746 && e.getX() < 834 && e.getY() > 850 && e.getY() < 876) {
        	this.activeDesign = savedDesignList[6].getModel();
        	modelPresentations.setActiveModel(savedDesignList[6].getModel());
        	this.c1 = savedDesignList[6].getColor(0); 
        	this.c2 = savedDesignList[6].getColor(1);
        	this.c3 = savedDesignList[6].getColor(2); 
        	this.c4 = savedDesignList[6].getColor(3);
        	repaint();
        	System.out.println("Editing Design 6");
        }  
        else if (e.getX() > 858 && e.getX() < 946 && e.getY() > 850 && e.getY() < 876) {
        	this.activeDesign = savedDesignList[7].getModel();
        	modelPresentations.setActiveModel(savedDesignList[7].getModel());
        	this.c1 = savedDesignList[7].getColor(0); 
        	this.c2 = savedDesignList[7].getColor(1);
        	this.c3 = savedDesignList[7].getColor(2); 
        	this.c4 = savedDesignList[7].getColor(3);
        	repaint();
        	System.out.println("Editing Design 7");
        }  
        else if (e.getX() > 970 && e.getX() < 1058 && e.getY() > 850 && e.getY() < 876) {
        	this.activeDesign = savedDesignList[8].getModel();
        	modelPresentations.setActiveModel(savedDesignList[8].getModel());
        	this.c1 = savedDesignList[8].getColor(0); 
        	this.c2 = savedDesignList[8].getColor(1);
        	this.c3 = savedDesignList[8].getColor(2); 
        	this.c4 = savedDesignList[8].getColor(3);
        	repaint();
        	System.out.println("Editing Design 8");
        }  
        else if (e.getX() > 1082 && e.getX() < 1170 && e.getY() > 850 && e.getY() < 876) {
        	this.activeDesign = savedDesignList[9].getModel();
        	modelPresentations.setActiveModel(savedDesignList[9].getModel());
        	this.c1 = savedDesignList[9].getColor(0); 
        	this.c2 = savedDesignList[9].getColor(1);
        	this.c3 = savedDesignList[9].getColor(2); 
        	this.c4 = savedDesignList[9].getColor(3);
        	repaint();
        	System.out.println("Editing Design 9");
        }  

        
        //// SAVE BUTTONS ////
        
        else if (e.getX() > 74 && e.getX() < 162 && e.getY() > 680 && e.getY() < 706) {
        	savedDesignList[0] = new Design(c1, c2, c3, c4, activeDesign);
        	modelPresentations.setSavedDesign0(activeDesign);
        	repaint();
        	System.out.println("Saved current design to slot 1");
        }       
        else if (e.getX() > 186 && e.getX() < 274 && e.getY() > 680 && e.getY() < 706) {
        	savedDesignList[1] = new Design(c1, c2, c3, c4, activeDesign);
        	modelPresentations.setSavedDesign1(activeDesign);
        	repaint();
        	System.out.println("Saved current design to slot 2");
        }
        else if (e.getX() > 298 && e.getX() < 386 && e.getY() > 680 && e.getY() < 706) {
        	savedDesignList[2] = new Design(c1, c2, c3, c4, activeDesign);
        	modelPresentations.setSavedDesign2(activeDesign);
        	repaint();
        	System.out.println("Saved current design to slot 3");
        }
        else if (e.getX() > 410 && e.getX() < 498 && e.getY() > 680 && e.getY() < 706) {
        	savedDesignList[3] = new Design(c1, c2, c3, c4, activeDesign);
        	modelPresentations.setSavedDesign3(activeDesign);
        	repaint();
        	System.out.println("Saved current design to slot 4");
        }
        else if (e.getX() > 522 && e.getX() < 610 && e.getY() > 680 && e.getY() < 706) {
        	savedDesignList[4] = new Design(c1, c2, c3, c4, activeDesign);
        	modelPresentations.setSavedDesign4(activeDesign);
        	repaint();
        	System.out.println("Saved current design to slot 5");
        }
        else if (e.getX() > 634 && e.getX() < 722 && e.getY() > 680 && e.getY() < 706) {
        	savedDesignList[5] = new Design(c1, c2, c3, c4, activeDesign);
        	modelPresentations.setSavedDesign5(activeDesign);
        	repaint();
        	System.out.println("Saved current design to slot 6");
        }
        else if (e.getX() > 746 && e.getX() < 834 && e.getY() > 680 && e.getY() < 706) {
        	savedDesignList[6] = new Design(c1, c2, c3, c4, activeDesign);
        	modelPresentations.setSavedDesign6(activeDesign);
        	repaint();
        	System.out.println("Saved current design to slot 7");
        }
        else if (e.getX() > 858 && e.getX() < 946 && e.getY() > 680 && e.getY() < 706) {
        	savedDesignList[7] = new Design(c1, c2, c3, c4, activeDesign);
        	modelPresentations.setSavedDesign7(activeDesign);
        	repaint();
        	System.out.println("Saved current design to slot 8");
        }
        else if (e.getX() > 970 && e.getX() < 1058 && e.getY() > 680 && e.getY() < 706) {
        	savedDesignList[8] = new Design(c1, c2, c3, c4, activeDesign);
        	modelPresentations.setSavedDesign8(activeDesign);
        	repaint();
        	System.out.println("Saved current design to slot 9");
        }
        else if (e.getX() > 1082 && e.getX() < 1170 && e.getY() > 680 && e.getY() < 706) {
        	savedDesignList[9] = new Design(c1, c2, c3, c4, activeDesign);
        	modelPresentations.setSavedDesign9(activeDesign);
        	repaint();
        	System.out.println("Saved current design to slot 10");
        }

        
		//// CLICK TO CHANGE ACTIVE MODEL PATTERN ////	
        
        else if (e.getX() > 120 && e.getX() < 180 && e.getY()>80 && e.getY()<160) {
        	this.activeDesign = ModelShape.MODELS[0];
        	modelPresentations.setActiveModel(ModelShape.MODELS[0]);
        	repaint();
        }
        else if (e.getX() > 190 && e.getX() < 250 && e.getY()>80 && e.getY()<160) {
        	this.activeDesign = ModelShape.model2;
        	modelPresentations.setActiveModel(ModelShape.MODELS[1]);
        	repaint();
        }
        else if (e.getX() > 260 && e.getX() < 320 && e.getY()>80 && e.getY()<160) {
        	this.activeDesign = ModelShape.model3;
        	modelPresentations.setActiveModel(ModelShape.MODELS[2]);
        	repaint();
        }    
        else if (e.getX() > 330 && e.getX() < 390 && e.getY()>80 && e.getY()<160) {
        	this.activeDesign = ModelShape.model4;
        	modelPresentations.setActiveModel(ModelShape.MODELS[3]);
        	repaint();
        }
        else if (e.getX() > 400 && e.getX() < 460 && e.getY()>80 && e.getY()<160) {
        	this.activeDesign = ModelShape.model5;
        	modelPresentations.setActiveModel(ModelShape.MODELS[4]);
        	repaint();
        }
        else if (e.getX() > 470 && e.getX() < 530 && e.getY()>80 && e.getY()<160) {
        	this.activeDesign = ModelShape.model6;
        	modelPresentations.setActiveModel(ModelShape.MODELS[5]);
        	repaint();
        }

        
        //// CLICK TO SET USER SELECTED COLORS ////
        
        else if (e.getX() > 126 && e.getX() < 176 && e.getY()>201 && e.getY()<251) {
        	this.c1 = activeColor;
        	repaint();
        }
        else if (e.getX() > 183 && e.getX() < 233 && e.getY()>201 && e.getY()<251) {
        	this.c2 = activeColor;
        	repaint();
        }
        else if (e.getX() > 240 && e.getX() < 290 && e.getY()>201 && e.getY()<251) {
        	this.c3 = activeColor;
        	repaint();
        }
        else if (e.getX() > 297 && e.getX() < 347 && e.getY()>201 && e.getY()<251) {
        	this.c4 = activeColor;
        	repaint();
        }
        
        
        //// COLORSWATCH LINE 1 ////
        
        else if (e.getX() > 35 && e.getX() < 55 && e.getY() > 272 && e.getY() < 292) {
        	this.activeColor = (c11);
        	repaint();
        }
        else if (e.getX() > 55 && e.getX() < 75 && e.getY() > 272 && e.getY() < 292) {
        	this.activeColor = (c12);
        	repaint();
        }
        else if (e.getX() > 75 && e.getX() < 95 && e.getY() > 272 && e.getY() < 292) {
        	this.activeColor = (c13);
        	repaint();
        }
        else if (e.getX() > 95 && e.getX() < 115 && e.getY() > 272 && e.getY() < 292) {
        	this.activeColor = (c14);
        	repaint();
        }
        else if (e.getX() > 115 && e.getX() < 135 && e.getY() > 272 && e.getY() < 292) {
        	this.activeColor =(c15);
        	repaint();
        }
        else if (e.getX() > 135 && e.getX() < 155 && e.getY() > 272 && e.getY() < 292) {
        	this.activeColor = (c16);
        	repaint();
        }
        else if (e.getX() > 155 && e.getX() < 175 && e.getY() > 272 && e.getY() < 292) {
        	this.activeColor = (c17);
        	repaint();
        }        
        else if (e.getX() > 175 && e.getX() < 195 && e.getY() > 272 && e.getY() < 292) {
        	this.activeColor = (c18);
        	repaint();
        }
        else if (e.getX() > 195 && e.getX() < 215 && e.getY() > 272 && e.getY() < 292) {
        	this.activeColor = (c19);
        	repaint();
        }
        else if (e.getX() > 215 && e.getX() < 235 && e.getY() > 272 && e.getY() < 292) {
        	this.activeColor = (c10);
        	repaint();
        }
        
        
        //// COLORSWATCH LINE 2 ////
        
        else if (e.getX() > 35 && e.getX() < 55 && e.getY() > 296 && e.getY() < 316) {
        	this.activeColor = (c21);
        	repaint();
        }
        else if (e.getX() > 55 && e.getX() < 75 && e.getY() > 296 && e.getY() < 316) {
        	this.activeColor = (c22); 
        	repaint();
        }
        else if (e.getX() > 75 && e.getX() < 95 && e.getY() > 296 && e.getY() < 316) {
        	this.activeColor = (c23);
        	repaint();
        }
        else if (e.getX() > 95 && e.getX() < 115 && e.getY() > 296 && e.getY() < 316) {
        	this.activeColor = (c24); 
        	repaint();
        }
        else if (e.getX() > 115 && e.getX() < 135 && e.getY() > 296 && e.getY() < 316) {
        	this.activeColor =(c25);
        	repaint();
        }
        else if (e.getX() > 135 && e.getX() < 155 && e.getY() > 296 && e.getY() < 316) {
        	this.activeColor = (c26);
        	repaint();
        }
        else if (e.getX() > 155 && e.getX() < 175 && e.getY() > 296 && e.getY() < 316) {
        	this.activeColor = (c27);
        	repaint();
        }        
        else if (e.getX() > 175 && e.getX() < 195 && e.getY() > 296 && e.getY() < 316) {
        	this.activeColor = (c28);
        	repaint();
        }
        else if (e.getX() > 195 && e.getX() < 215 && e.getY() > 296 && e.getY() < 316) {
        	this.activeColor = (c29);
        	repaint();
        }
        else if (e.getX() > 215 && e.getX() < 235 && e.getY() > 296 && e.getY() < 316) {
        	this.activeColor = (c20);
        	repaint();
        }
                       
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
	}

}
