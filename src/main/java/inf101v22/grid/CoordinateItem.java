package inf101v22.grid;

import java.util.Objects;

public class CoordinateItem <E> {
	
	public final E item;
	public final Coordinate coordinate;
	
	/**
	 * CoordinateItem links a coordiante to an item value 
	 */
	public CoordinateItem(Coordinate coordinate, E item) {
		this.item = item;
		this.coordinate = coordinate;
	}

	/**
	 * @return the item value of the current coordinate
	 */
	public E get() {
		return this.item;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(coordinate, item);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoordinateItem<?> other = (CoordinateItem<?>) obj;
		return Objects.equals(coordinate, other.coordinate) && Objects.equals(item, other.item);
	}

}
