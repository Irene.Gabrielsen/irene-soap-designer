package inf101v22.grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Grid<E> implements IGrid<E>{
	
	private final int rows;
	private final int cols;
	private final List<E> grid;
	
	/** 
	 * Grid creates an underlying datastructure to build on.
	 * This version takes an initial value and fills the grid array with this value.
	 * 
	 * @param rows
	 * @param cols
	 * @param initialValue
	 */
	public Grid(int rows, int cols, E initialValue){
		if (rows <= 0 || cols <= 0) {
			throw new IllegalArgumentException();
		}
		this.rows = rows;
		this.cols = cols;
		grid = new ArrayList<E>(rows * cols);

		for (int i=0; i < rows*cols; ++i) {
			grid.add(initialValue);
		}
	}	
	
	/**
	 * A grid constructor with no initial value.
	 * 
	 * @param rows
	 * @param cols
	 */
	public Grid(int rows, int cols) {		
		this.rows = rows;
		this.cols = cols;
		grid = new ArrayList<E>(rows*cols);
	}
	
	/**
	 * indexOf() takes a grid coordinate and calculets its index in the grid array
	 * 
	 * @param coordinate
	 * @return the index corresponding to given coordinate
	 */
	private int indexOf(Coordinate coordinate) {		
		int row = coordinate.row;
		int col = coordinate.col;
		return col+row*cols;
	}
	
	@Override
	public Iterator<CoordinateItem<E>> iterator() {		
		ArrayList<CoordinateItem<E>> coordinates = new ArrayList<CoordinateItem<E>>(rows*cols);		
		for (int i=0; i<rows; ++i) {
			for (int j=0; j<cols; ++j) {
				Coordinate coordinate = new Coordinate(i, j);
				CoordinateItem<E> coordinateItem = new CoordinateItem<E>(coordinate, this.get(coordinate));
				coordinates.add(coordinateItem);
			}
		}
		return coordinates.iterator();
	}
	
	@Override
	public int getRows() {		
		return this.rows;
	}

	@Override
	public int getCols() {
		return this.cols;
	}
	
	@Override
	public void set(Coordinate coordinate, E value) {
		if (coordinateIsOnGrid(coordinate) == false) {
			throw new IndexOutOfBoundsException("Index out of bound");
		}
		else {
			grid.set(indexOf(coordinate), value);
		}
	}

	@Override
	public E get(Coordinate coordinate) {
		if (coordinateIsOnGrid(coordinate) == false) {
			throw new IndexOutOfBoundsException("Index out of bound");
		}
		else {
			return grid.get(indexOf(coordinate));
		}
	}
	
	@Override
	public boolean coordinateIsOnGrid(Coordinate coordinate) {
		if (coordinate.row >= this.rows || coordinate.row < 0) {
			return false;	
		}
		else if (coordinate.col >= this.cols || coordinate.col < 0) {
			return false;
		}
		else {
			return true;
		}
	}

}
