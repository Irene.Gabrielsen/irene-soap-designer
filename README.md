## SOAP DESIGNER

##### Innhold:
 - Motivasjon
 - Instrukser
 - Klassediagram
 - Forklaring til koden
 - Vurderingskriterier
 - Tester
 - Arbeidslogg

##### Motivasjonen for å lage Soap Designer:

Soap designer er et program for dem som lager hjemmelaget såpe, her er en veldig kort forklaring til dem som ikke kjenner til prosessen:
 - Man blander sammen en røre, som man kan sette farge på og helle i formen i ønsket mønster, her er det kun fantasien som setter grenser.
 - Når røren er blandet sammen, begynner klokken å tikke, og man må være ferdig med prosessen før røren stivner, noe som kan gå veldig fort - ned til et par minutter.
 - Derfor er det essensielt for såpemakere å ha hele planen klar før arbeidet begynner, inkludert skissering av ønsket mønster og farge.
 
 Alle kan bruke fargeblyanter eller et tegneprogram på PC-en for å finne farger som passer sammen, og skape et visuelt intrykk av sluttresultatet, men et verktøy spesielt egnet for dette er savnet. Det er ganske vanskelig å se for seg hva som blir fint, og ofte kan farge/mønster kombinasjoner fungere overraskende bra selv om det høres ut som en dårlig kombinasjon.
 
Siden man ikke orket å tegne opp for hånd (eller PC) noe man ikke ser for seg blir bra, prøver man som regel bare ut 4-5 design før man bestemmer seg. Soap Designer lar deg prøve ut hundrevis av kombinasjoner på få minutter.
 
##### Hvordan bruke Soap Designer:
 
I Soap Designer kan man enkelt klikke frem og tilbake mellom forskjellige mønster og farger, og se designet endre seg på skjermen.

Åpne og bruk programmet som følger:
 
 1) Gå til Main (/SoapDesigner/src/main/java/inf101v22/soapDesigner/Main.java)
 
 2) Kjør programmet
 
 3) Klikk på et mønster (øverst) for å velge det, default er det første mønsteret.
 
 4) Klikk på farger i fargeutvalget og deretter "SET" i en av de fire rutene som representerer de utvalgte fargene.
 
 5) Du kan nå følge med underveis hvordan mønsteret blir seende ut, og enkelt bytte ut både mønster og farger.
 
 6) Når du finner et design du liker, men ønsker å utforske videre, kan du klikke "Save here" i bunnen av vinduet, og designet blir lagret på denne plassen. Hvis et design allerede var lagret her, vil det nye designe erstatte det.

 7) Opp til 10 designs kan lagres i vinduet, og du kan redigere dem igjen ved å klikke "Edit Design" under valgt design.

 
##### Klassediagram:
 
 Et klassediagram ligger lagret som "Class Diagram oblig 2" på samme sted som README.
 Det er første gang jeg lager klassediagram selv, og noen av forbindelsene skulle kanskje vært annerledes, men det gir uansett et oversiktsbilde.
 

##### Forklaringer og kommentarer til koden:
 
Coordinate, CoordinateItem, Grid og IGrid er alle strukturer som mer eller mindre like Tetris spillets filer med samme navn. Dette er fordi jeg kopierte Tetris prosjektet som stamme til det nye prosjektet, grunnet vanskeligheter med å sette opp Java library på nytt. Men siden jeg ønsket å bruke en grid struktur som basis i det nye programmet, og jeg kjente godt til filene etter å ha laget dem selv i det første prosjektet, brukte jeg dem videre med noen små modifikasjoner.

Flere "backgroundBoard" opprettes for å holde på informasjonen jeg legger inn i dem. Jeg trenger 3 forskjellige bakgrunner i Soap Designer fordi størrelsen på gridets ruter er forskjellig. For eksempel er de små modellene på toppen laget med ruter som er 10x10, mens hoved designet i midten har ruter som er 50x50.

SoapView har mouseListener i seg, og utfører de endringene som skal skje mår musen klikkes. Jeg har ikke laget en egen kontroller fordi jeg laget et fungerende program og valgte å bruke tiden på å utvikle det ferdig. SoapDesigner er et så pass lite program at det går greit nok, men skal jeg utvide programmet vil jeg nok lage en egen kontroller på samme måte som i Tetris prosjektet.

De 10 lagrede designene på bunnen har alle to knapper hver, "Save Here" og "Edit Design", dette fører til at en del kode som er ganske lik må skrives inn i mouselicked. I tillegg finnes 10 setSavedDesign-metoder i ModelPresentations fordi hver save-knapp aktiverer sin egen setSavedDesign-metode. Jeg tror ikke jeg kunne gjort det annerledes i mouseClicked, siden alle 10 knappene må ha sitt egen definerte input område, men jeg kunne nok ha funnet en løsning på setSavedDesign metodene som finnes 10 av, ved å gi en indeks som parameter til metoden. Dette hadde blitt det neste å jobbe med hadde jeg hatt mer til på oppgaven.

##### Vurderingskriterier:

Når det gjelder gjenbruk har jeg prøvd å gjøre det jeg kan gitt de behovene programmet har. BackgroundBoard og Grid kan brukes til å sette opp hva som helst uten at det påvirker resten av koden, Coordinate, Pixel, Design og ModelShapes er alle objekter som kan brukes av flere forskjellige program og metoder.

Når grafikken skal tegnes bruker draw-metodene en hjelpemetode, den bestemmer om det skal være farger eller svart-hvitt, og hvilke fargeutvalg som skal brukes. En annen hjelpemetode jeg laget gjorde det mer oversiktlig å tegne knappene, den heter drawButton. DrawButton tar inn størrelse-, lokasjons- og farge-parametre som bestemmer hvor knappen blir tegnet, hvor stor den er, og hvilken farge det er på rammen og i midten. Dette sparte meg for mangen linjer med kode ettersom jeg har over 20 knapper i vinduet.

Jeg har skrevet programmet slik at det skal være ganske enkelt å legge til flere ModelShape objekter, uten å måtte endre på ting flere steder i koden. Slik det er nå må man legge til kode på 2 plasser; i selve ModelShape filen, og i ModelPresentations der den må føres opp på listen over modeller som tegnes på BackgroundBoard (Det kunne eventuelt blitt gjort slik at alle ModelShapes i listen ble lagt til sitt BackgroundBoard automatisk, meg jeg får ikke tid til å endre på det nå).

Polymorphisme er det begrenset behov for slik programmet er nå, men BackgroundBoard og Grid faller begge i den kategorien. Eventuelt kunne programmet blitt utvidet til å designe andre typer objecter med andre former og farger, og da vil de nåværende klassene for modeller, design og piksler helt fint kunne brukes til det, men for eksempel bli satt på en annerledes bakgrunn, eller ha en annen fargepalett tilgjengelig osv.

Jeg har tatt i bruk generisk type for å kunne gruke grid og koordinater til annet enn piksler til Soap Designer programmet.

Modulariteten til programmet blir hjulpet av grensesnitt, men noen filer, for eksempel SoapView og ModelPresentation, snakker fortsatt sammen direkte fordi jeg gjorde endringer underveis i prosjektet, og tiden begynnte å gå fort. Ellers i programmet tror jeg ting er greit isolert og jeg har unngått public og statiske metoder og variabler så godt jeg kan.


##### Tester:

Tester for grid, coordiante og coordinateItem er hentet fra Tetris prosjektet. Dette er fordi filene selv er like som i Tetris, så jeg valgte å ikke fjerne dem selv om jeg ikke har skrevet dem selv (De lå inne for forrige semester oppgave).

Jeg la selv til to tester for SoapView, en som tester om SoapView klarer å lagre designs i savedDesignList, og en som sjekker at shuffle metoden faktisk bytter om på fargevariablenes farger slik den skal.

Veldig mange ting i mitt program kan testes manuelt/visuelt ved å ta programmet i bruk. Jeg har laget en egen fil med oversikt over hva som ble testet på denne måten, den heter "Manuelle tester" og ligger på samme sted som README.

##### Arbeidslogg:

En arbeidslogg ligger lagret på samme sted som README, i den skrev jeg hva planen min var i begynnelsen, og ukentlig hva jeg hadde oppnådd. Det er ikke sikkert dette er nyttig, men den ligger der hvis det trengs.



