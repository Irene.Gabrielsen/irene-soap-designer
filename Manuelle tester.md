# Manuelle tester:

##### Endre aktiv farge:
Når man klikker på en farge på paletten skal activeColor variabelen endres, og tilsvarende farge skal fylles i fargeruten øverst, til venstre for de 4 utvalgte fargene.

 - Jeg klikker igjennom de 20 fargene en-etter-en, og bekrefter at den aktive fargen er lik fargen jeg trykket på i paletten.
 
##### Bestemme de 4 fargene for den aktive designen:
Når man har en aktiv farge kan man klikke på en av de 4 rutene med "SET", for å velge den aktive fargen og bruke den i designet. Designet skal oppdatere seg med en gang en farge velges.

 - Jeg velger 4 vilkårlige farger og setter dem i hver sin rute, den aktive designen i midten endrer seg umiddelbart etterhvert.
 - Jeg velger flere sett, og prøver også ut å ha identiske farger i flere ruter, samt å ikke endre farge i noen. (Alle rutene har en default verdi, så de er aldri fargeløse)

##### Bytte aktiv modell:
Når man klikker på en av de 6 modellene i toppen, skal designet i midten bytte om til å bruke modellen du har klikket på.

 - Jeg velger 4 farger for å kunne se hvilken modell som blir valgt, og klikker på alle 6 modelle for å bekrefte at programmet bytter om til riktig modell. 
 - Jeg prøver også å klikke helt i kanten av hvert modellbilde, for å sjekke at hele området er innenfor rekkevidden til mouseClicked.
 
##### Lagre designet:
Når man trykker på en av "Save Here" knappene, skal det aktive designet bli lagret på plassen rett under knappen.

 - Jeg velger 4 vilkårlige farger og en modell, så klikker jeg på en av lagre knappene og ser at akkurat det designet jeg laget blir lagret.
 - Jeg endrer så vilkårlig på farger og modell mens jeg lagrer de forskjellige designene i resten av de ledige plassene. Noen design er like, andre delvis lik, mens resten helt forskjellig fra hverandre.
 - Når alle plassene er okkupert av et lagret design, lager jeg enda et nytt design og klikker "Save Here" der det allerede finnes et lagret design. Det nye designet overskriver det gamle, og dette er intensjonen min fordi jeg ikke ville at bruke skulle måtte slette det som allerede er på plassen før de kan lagre ett nytt design. (Unødvendig mye knapper)

##### Endre et lagret design:
Når man trykker "Edit Design", skal den aktive designen i midten endres til fargene og modellen som den lagrede designen over knappen har.

 - Jeg klikker "Edit Design" på en av de lagrede designene og ser at bildet i midten blir likt det lagrede designet jeg valgte. De 4 rutene med valgte farger ble også oppdatert.
 - Jeg klikker frem og tilbake på de forskjellige "Edit Design" knappene for å se at alle fungerer, og at designet i midten endres for hver gang.


