# Plan og utførelse

# Ide:
 - Jeg skal lage et program som hjelper deg å designe såpe (eller hva enn du har lyst til å designe). Programmet er nyttig fordi når man lager såpe må alt være planlagt på forhånd, man kan verken se resultatet underveis, eller justere farge/design. Dette programmet gjør det mulig å prøve ut et mye bredere utvalg farger og design enn du ville fått til med noen fargeblyanter og et ark, og på en lettere måte enn med et generisk tegneprogram.

# Plan:

 - Jeg vil ha et aktivt design i midten av vinduet, og en rekke mønster å velge blandt øverst i vinduet. Ved siden av designet skal det være en fargepalett der man velger de aktive fargene. Jeg vil at man skal kunne lagre designene underveis for å kunne sammenligne dem og eventuelt gå tilbake å redigere tidligere design, og at de skal lagres synlig på en linje nederst i vinduet.
 
  - I tillegg vil jeg ha en knapp for å shuffle fargene i tilfeldig rekkefølge, en knapp for å vise en utskriftsvennlig side av ditt valgte design, og en inputboks til å gi et navn til designen.

# Arbeidslogg:

##### Uke 1, 4-8 april:

Utviklet ideen min og snakket med gruppeleder angående hva som er reelt å få til, og hvordan starte prosjektet.

Jeg brukte de resterende dagene på å sette opp hovedvinduet, og få det til å lese inn ett mønster. Dette er ganske likt måten Tetris tegnet en brikke i det aktive vinduet, og jeg vil bruke et grid med små ruter som fylles opp av farger, slik at de fungerer som piksler.

Jeg fikk også satt opp en linje med tilgjengelige mønster øverst i vinduet, samt vise det aktive designet i midten

##### Uke 2, 11-15 april (Påske):

I begynnelsen av denne uken laget jeg en fargepalett av typen "colorswatches", som er et utvalg farger man kan velge mellom ved å klikke på. Jeg ønsket en fullstendig egendefinert palett, men det får jeg jobbe med hvis det blir tid til overs.

Jeg implementerte mouseListener i hovedvinduet, og gjorde det mulig å endre de aktive fargene ved å klikke på paletten.

##### Uke 3, 18-22 april:

I denne uken fokuserte jeg på å kunne lagre designene i bunnen av vinduet. Jeg har tenkt å la bruker lagre opp til 10 design om gangen, og også la dem slette eller redigere dem.

Jeg fikk hjelp av gruppeleder til å finne den beste måten å lagre designene, som er en array med 10 objekter i. Hvert objekt er av typen Design, som holder på 4 farger og ett mønster (model).

Resten av dagene gikk med på å skrive om koden en del for å fjerne steder med identisk kode, samle kodesnutter, og gjøre navn på metoder, klasser og variabler mer intuitive. Jeg fjernet også en god del "static" tilganger som var unødvendige, og gjorde flere metoder "private".

##### Uke 4, 25-29 april:

Etter å ha skrevet om koden for å fjerne duplikat koder, har jeg nå fått det aktive designet i midten til å fungere som det skal igjen. Jeg klarte også å lage regnestykker som viser forskjellige farge-layouts basert på hvilke kolonner det lagrede designet blir tegnet i, dermed har jeg nå muligheten til å lagre 10 design på bunnen av siden slik som planlagt. I tillegg gjorde jeg det mulig å hente frem igjen et lagret design ved å klikke "Edit Design" under det valgte designet.

Nå som tiden er ute, er jeg veldig fornøyd med at programmet fungerer helt som planlagt!

Jeg hadde håpt å kunne skrive inn et navn på designet, og vise en utskriftsvennlig side, men det ble det ikke tid til. Jeg brukte heller den siste tiden til å gjøre eksisterende kode bedre.

##### I fremtiden:

Siden jeg planlegger å bruke programmet selv når jeg lager såpe, kommer jeg til å prøve å utvikle det videre. Som du ser er det plass til mange flere fargepaletter på venstre siden, mens på høyre side vil jeg implementere input for navn på designet, knapp til den utskriftsvennlige siden, og input for støpeformens mål slik at programmet kan regne ut areal og volum for brukeren, så de vet hvor mye ingredienser de trenger.


